(function($) {
  $(document).ready(function() {
    // If admin menu div exist.
    if ($('body').hasClass('admin-menu')) {
      // Ajax request content of admin menu.
      $.getJSON(Drupal.settings.basePath, { q: Drupal.settings.pathPrefix + 'js/ofed_admin_menu/get_menu' }, function(data) {
        // If the server answers something
        if (data != null) {
          $('#admin-menu').replaceWith(data);
          var path = Drupal.settings.admin_menu.destination.slice(12).split('/', 2);
          path = path[0] + '/' + path[1];
          $('#admin-menu a[href="/' + Drupal.settings.pathPrefix + path + '"]').addClass('active-trail');
        }
        // Display the menu
        $('#admin-menu').css('position', 'fixed');
        $('#admin-menu').css('visibility', 'visible');
        $('#admin-menu-wrapper').fadeIn();
      });
    }
  })
})(jQuery);

Introduction
------------
This module allows one to force the language used by the Administration menu
module by reloading it in ajax.


Limitations
-----------
Only tested in the "OpenFed / Fast2web D7" distribution.
Normally, it should work as a standalone but it was developed in the scope of
"OpenFed / Fast2web D7" distribution so it's may be not the case.
So for the moment it's very specific to "OpenFed / Fast2web D7" distribution.
If you catch an error, don't forget to report in the issues.

Credits
-------
Developed by Blue4You sponsored by Fedict.
